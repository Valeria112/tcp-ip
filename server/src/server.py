#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket
from constants import COMMANDS as cm
from server_utils import *

def serve(sock):
	# приём подключения 
	# новый сокет и адрес клиента
	conn, addr = sock.accept()

	print 'connected:', addr

	data = conn.recv(1024)
	if data == cm.START_CLIENT:
		conn.send(cm.QUERY_NAME)

	user_name = conn.recv(1024)
	user_id = add_user_if_not_exist(user_name)

	answer = x = 1
	points = -1

	while answer == x:
		words, answer = get_three_words()
		conn.send(words[0]+'\n'+words[1]+'\n'+words[2])
		x = conn.recv(1024)
		points += 1
	
	set_result_by_user_id(user_id, points)
	conn.send(cm.WRONG_ANSWER)

	answer = ''
	while answer != cm.STOP_CLIENT:
		answer = conn.recv(1024)
		if answer == cm.GET_RESULT:
			conn.send(str(points))
		elif answer == cm.BEST_RESULT:
			best_res = ''
			for line in get_best_results(3):
				best_res += line[0] + '\t' + str(line[1]) + '\n'
			conn.send(best_res)

	conn.close()

def main():
	# создание сокета
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# устанавливаем хост и порт
	sock.bind(('', 9090))
	# определяет максимальный размер очереди
	sock.listen(1)

	while True:
		serve(sock)

if __name__ == '__main__':
	main()

