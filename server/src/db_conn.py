import psycopg2
from constants import *

tn = DB_SETTINGS.TABLE_NAMES
cn = DB_SETTINGS.COLUMN_NAMES

# database connor
class DB:
    conn = None

    def connect(self):
        self.conn = psycopg2.connect(
        	database=DB_SETTINGS.NAME, 
        	user=DB_SETTINGS.USER, 
        	host=DB_SETTINGS.HOST, 
        	password=DB_SETTINGS.PASSWORD)

    def query(self, sql):
        try:
          cursor = self.conn.cursor()
          cursor.execute(sql)
        except (AttributeError, psycopg2.Error):
          self.connect()
          cursor = self.conn.cursor()
          cursor.execute(sql)
        return cursor

    def transaction(self, queries):
        success = True
        try:
            for sql in queries:
                self.query(sql)
            self.conn.commit()
        except psycopg2.Error:
            self.conn.rollback()
            success = False
        return success

    def disconnect(self):
        if self.conn:
            self.conn.close()