class COMMANDS:
    START_CLIENT = 'start'
    QUERY_NAME ='name'
    WRONG_ANSWER = 'wrong answer'
    STOP_CLIENT = 'stop'
    GET_RESULT = 'result'
    BEST_RESULT = 'best_res'

class DB_SETTINGS:
    NAME = 'test_python'
    USER = 'test_user'
    HOST = 'localhost'
    PASSWORD = 'test_password'

    class TABLE_NAMES:
    	USERS_TABLE = 'users'
    	RESULTS_TABLE = 'results'

    	CATEGORIES_TABLE = 'categories'
        ANIMALS_TABLE = 'animals'
        COLORS_TABLE = 'colors'
        COUNTRIES_TABLE = 'countries'
        FURNITURE_TABLE = 'furniture'
        PLANETS_TABLE = 'planets'
        PLANTS_TABLE = 'plants'
        SPORTS_TABLE = 'sports'
        TECHNOLOGY_TABLE = 'technology'
        WEAPONS_TABLE = 'weapons'
        WEATHER_TABLE = 'weather'

    class COLUMN_NAMES:
        USERS_COLUMNS = ['user_id', 'user_name']
        RESULTS_COLUMNS = ['user_id', 'best_result']

        CATEGORIES_COLUMNS = ['category_id', 'category_name']
        CATEGORY_COLUMNS = ['id', 'name']