# -*- coding: utf-8 -*-
from db_conn import *
import random as rand


def select_random_values(rows, nums_val=1):
	values = []
	while len(values) != nums_val:
		num = rand.randint(0, len(rows) - 1)
		val = rows[num]
		if not val in values:
			values.append(val)
	return values


def get_names_by_table(table_name, num_words=1):
	db = DB()
	cursor = db.query("SELECT * FROM {0};".format(table_name))
	rows = []
	for row in cursor:
		rows.append(row[1])
	db.disconnect()
	return select_random_values(rows, num_words)


def random_mix_lists(list1, list2):
	k = rand.randint(0, 2)
	if k == 0:
		return [list1[0], list2[0], list1[1]]
	elif k == 1:
		return [list1[1], list1[0], list2[0]]
	elif k == 2:
		return [list2[0], list1[0], list1[1]]


def get_three_words():
	categories = get_names_by_table(tn.CATEGORIES_TABLE, 2)
	two_words = get_names_by_table(categories[0], 2)
	word = get_names_by_table(categories[1])
	answer = word[0]
	words = random_mix_lists(two_words, word)

	return words, answer


def add_user_if_not_exist(user_name):
	db = DB()
	cursor = db.query("SELECT * FROM {0} WHERE {1}='{2}';"
		.format(tn.USERS_TABLE, cn.USERS_COLUMNS[1], user_name) )
	rows = []
	for row in cursor:
		rows.append(row[0])

	if rows:
		user_id = rows[0]
	else:
		sql1 = "INSERT INTO {0}({1}) VALUES('{2}')".\
			format(tn.USERS_TABLE, cn.USERS_COLUMNS[1], user_name)
		db.transaction([sql1]) # проверка, что транзакция прошла
		cursor = db.query("SELECT {0} FROM {1} WHERE {2}='{3}';"
		.format(cn.USERS_COLUMNS[0], tn.USERS_TABLE, cn.USERS_COLUMNS[1], user_name) )
		for row in cursor:
			user_id = row[0]
	db.disconnect()
	return user_id


def set_result_by_user_id(user_id, result):
	db = DB()
	cursor = db.query("SELECT * FROM {0} WHERE {1}='{2}';"
		.format(tn.RESULTS_TABLE, 
			cn.RESULTS_COLUMNS[0], user_id) )
	rows = []
	for row in cursor:
		rows.append(row[1])

	if rows:
		if result > rows[0]:
			sql = "UPDATE {0} SET {1}={2} WHERE {3}={4};".\
			format(tn.RESULTS_TABLE, 
				cn.RESULTS_COLUMNS[1], result, 
				cn.RESULTS_COLUMNS[0], user_id)
		else:
			db.disconnect()
			return
	else:
		sql = "INSERT INTO {0} VALUES({1},{2});".\
		format(tn.RESULTS_TABLE, user_id, result)

	db.transaction([sql]) # проверка, что транзакция прошла
	db.disconnect()


def get_best_results(num_results=1):
	db = DB()
	cursor = db.query("SELECT {0}, {1} FROM {2} INNER JOIN {3} USING({4}) \
		ORDER BY {1} DESC LIMIT {5};"
		.format(cn.USERS_COLUMNS[1], cn.RESULTS_COLUMNS[1], 
			tn.RESULTS_TABLE, tn.USERS_TABLE,
			cn.RESULTS_COLUMNS[0], num_results))
	rows = []
	for row in cursor:
		rows.append(row)

	db.disconnect()
	return rows
