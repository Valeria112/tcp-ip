--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: animals; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE animals (
    id integer,
    name text
);


ALTER TABLE public.animals OWNER TO test_user;

--
-- Name: auto_id_users; Type: SEQUENCE; Schema: public; Owner: test_user
--

CREATE SEQUENCE auto_id_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_users OWNER TO test_user;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE categories (
    category_id integer NOT NULL,
    category_name text
);


ALTER TABLE public.categories OWNER TO test_user;

--
-- Name: colors; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE colors (
    id integer,
    name text
);


ALTER TABLE public.colors OWNER TO test_user;

--
-- Name: countries; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE countries (
    id integer,
    name text
);


ALTER TABLE public.countries OWNER TO test_user;

--
-- Name: furniture; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE furniture (
    id integer,
    name text
);


ALTER TABLE public.furniture OWNER TO test_user;

--
-- Name: planets; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE planets (
    id integer,
    name text
);


ALTER TABLE public.planets OWNER TO test_user;

--
-- Name: plants; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE plants (
    id integer,
    name text
);


ALTER TABLE public.plants OWNER TO test_user;

--
-- Name: results; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE results (
    user_id integer,
    best_result integer
);


ALTER TABLE public.results OWNER TO test_user;

--
-- Name: sports; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE sports (
    id integer,
    name text
);


ALTER TABLE public.sports OWNER TO test_user;

--
-- Name: technology; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE technology (
    id integer,
    name text
);


ALTER TABLE public.technology OWNER TO test_user;

--
-- Name: users; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE users (
    user_id integer DEFAULT nextval('auto_id_users'::regclass) NOT NULL,
    user_name text
);


ALTER TABLE public.users OWNER TO test_user;

--
-- Name: weapons; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE weapons (
    id integer,
    name text
);


ALTER TABLE public.weapons OWNER TO test_user;

--
-- Name: weather; Type: TABLE; Schema: public; Owner: test_user; Tablespace: 
--

CREATE TABLE weather (
    id integer,
    name text
);


ALTER TABLE public.weather OWNER TO test_user;

--
-- Data for Name: animals; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY animals (id, name) FROM stdin;
1	Кошка
2	Собака
3	Мышь
4	Лев
5	Тигр
6	Енот
7	Крыса
8	Бурундук
9	Олень
10	Пингвин
11	Дельфин
\.


--
-- Name: auto_id_users; Type: SEQUENCE SET; Schema: public; Owner: test_user
--

SELECT pg_catalog.setval('auto_id_users', 8, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY categories (category_id, category_name) FROM stdin;
1	colors
2	animals
3	furniture
4	plants
5	weather
6	countries
7	planets
8	technology
9	weapons
10	sports
\.


--
-- Data for Name: colors; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY colors (id, name) FROM stdin;
1	Красный
2	Зелёный
3	Синий
4	Белый
5	Чёрный
6	Жёлтый
7	Оранжевый
8	Фиолетовый
10	Розовый
11	Коричневый
9	Голубой
\.


--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY countries (id, name) FROM stdin;
1	Россия
7	Англия
8	Германия
9	Греция
10	Бразилия
11	Ирак
6	Франция
5	Украина
2	США
3	Китай
4	Япония
\.


--
-- Data for Name: furniture; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY furniture (id, name) FROM stdin;
1	Стул
2	Стол
3	Кровать
4	Кресло
5	Шкаф
6	Тумбочка
\.


--
-- Data for Name: planets; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY planets (id, name) FROM stdin;
1	Меркурий
2	Венера
3	Земля
4	Марс
5	Юпитер
6	Сатурн
9	Плутон
8	Нептун
7	Уран
\.


--
-- Data for Name: plants; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY plants (id, name) FROM stdin;
1	Дуб
2	Сосна
3	Ель
4	Берёза
5	Рябина
\.


--
-- Data for Name: results; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY results (user_id, best_result) FROM stdin;
2	2
3	3
5	0
1	21
6	0
7	5
8	8
\.


--
-- Data for Name: sports; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY sports (id, name) FROM stdin;
1	Бег
2	Спортивная гимнастика
3	Карате
4	Фехтование
5	Плавание
6	Фигурное катание
7	Аэробика
8	Прыжки
9	Бадминтон
10	Баскетбол
11	Волейбол
\.


--
-- Data for Name: technology; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY technology (id, name) FROM stdin;
1	Телевизор
2	Радио
3	Рация
4	Телефон
5	Компьютер
6	Планшет
7	Домофон
8	Ноутбук
9	Нетбук
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY users (user_id, user_name) FROM stdin;
1	Валерия
2	Дмитрий
3	Ринат
5	Lera
6	
7	Юля
8	Антон
\.


--
-- Data for Name: weapons; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY weapons (id, name) FROM stdin;
1	Нож
2	Пистолет
3	Лук
4	Арбалет
5	Граната
6	Торпеда
7	Бомба
8	Ружьё
9	Мечь
10	Стрела
11	Карабин
12	Мушкет
13	Пулемёт
14	Пушка
\.


--
-- Data for Name: weather; Type: TABLE DATA; Schema: public; Owner: test_user
--

COPY weather (id, name) FROM stdin;
1	Снег
2	Дождь
3	Град
4	Гроза
5	Метель
6	Ливень
7	Радуга
8	Шторм
9	Смерчь
10	Вьюга
\.


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: test_user; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: results_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: test_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

