#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 9090))

sock.send('start')

if sock.recv(1024) == 'name':
	user_name = raw_input('Введите ваше имя:')
	sock.send(user_name)

words = sock.recv(1024)

while words != 'wrong answer':
	print words
	x = raw_input('Введи лишнее слово:')
	sock.send(x)
	words = sock.recv(1024)

sock.send('result')
print 'Неправильный ответ. Ваш результат {0} очков!\
\nИгра окончена...\n'.format(sock.recv(1024))

sock.send('best_res')
print 'Лучшие результаты:{0}\n'.format(sock.recv(1024))

sock.send('stop')
sock.close()